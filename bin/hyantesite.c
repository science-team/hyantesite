/*
 * This file is part of hyantes.
 *
 * hyantes is free software; you can redistribute it and/or modify
 * it under the terms of the CeCILL-C License
 *
 * You should have received a copy of the CeCILL-C License
 * along with this program.  If not, see <http://www.cecill.info/licences>.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <float.h>
#include <math.h>
#include <assert.h>

#ifndef S_SPLINT_S
#include <unistd.h>
#endif

#include "hyantes.h"

#include "hs_config.h"
#if defined HAVE_LIBPTHREAD && HAVE_LIBPTHREAD
#include <pthread.h>

static int display_status(int *sz)
{
    unsigned long max = (unsigned long) *sz;
    unsigned long curr = 0;
    while((curr = hs_status()) == 0)
        (void) sleep(1);
    while((curr = hs_status()) < max)
    {
        fprintf(stdout, "\r# completed task / total task:%lu/%lu [%d%%] ",
           curr, max, (int) (curr * 100 / max));
        if(fflush(stdout) != 0)
        {
            perror("[fflush]");
            exit(EXIT_FAILURE);
        }
        (void) sleep(1);
    }
    fprintf(stdout, "\n");
    return 0;
}
#endif
/******************************************************************************\
 * 
 *      option parsing
 *
 *******************************************************************************/
/*@-nullassign@*/
static struct option long_options[] = {
    {"dump-tree", 0, NULL, (int) 'd'},
    {"function", 1, NULL, (int) 'f'},
    {"help", 0, NULL, (int) 'h'},
    {"in", 1, NULL, (int) 'i'},
    {"out", 1, NULL, (int) 'o'},
    {"precomputed-file", 0, NULL, (int) 'p'},
    {"range", 1, NULL, (int) 'r'},
    {"scale", 1, NULL, (int) 's'},
    {"threshold", 1, NULL, (int) 't'},
    {"version", 0, NULL, (int) 'v'},
    {"window", 1, NULL, (int) 'w'},
    {NULL, 0, NULL, 0}
};

static char *long_description[] = {
    "require the dump of processed input. No further computation is done",
    "smoothing function name",
    "display this help and exit",
    "input file, default is stdin",
    "output file, default is stdout",
    "indicate that input file is precomputed",
    "range of the smoothing function, in kilometers",
    "scale of the output, given as LatxLon",
    "threshold used to skip small area",
    "display version info and exit",
    "visualization window, given as mlat,mlon,Mlat,Mlon"
};

static void print_version(FILE * out)
{
    fprintf(out, "%s\n", PACKAGE_STRING);
}

static void print_help(FILE * out)
{
    const size_t n = sizeof(long_description) / sizeof(*long_description);
    size_t i;
    print_version(out);
    fprintf(out, "\n%s [options]\n", PACKAGE);
    for(i = 0; i < n; i++)
    {
        if(long_options[i].val > 0)
            fprintf(out, "\t-%c,--%s\t: %s\n", long_options[i].name[0],
               long_options[i].name, long_description[i]);
        else
            fprintf(out, "\t  --%s\t: %s\n", long_options[i].name,
               long_description[i]);
    }
}

/******************************************************************************\
*
*               main
*
\******************************************************************************/

static int run(int argc, char **argv)
{
    size_t i;
    int resLat = 0, resLon = 0;
    hs_potential_t *pOut = NULL;
    hs_coord_t visu = { 0., 0., 0., 0. };
    char *function = NULL;
    const char default_function[] = "disk";
    double range = 0.;
    FILE *fi = NULL, *fo = NULL;
    int do_compute = 1;

#if defined HAVE_LIBPTHREAD && HAVE_LIBPTHREAD
    pthread_t thread;
    int sz;
#endif

    int opt = 0, option_index = 0;
    char short_options[1 +
       3 * sizeof(long_description) / sizeof(*long_description)];
    {
        size_t k;
        const size_t n =
           sizeof(long_description) / sizeof(*long_description);
        for(i = 0, k = 0; i < n; i++)
        {
            if(long_options[i].val > 0)
            {
                short_options[k++] = long_options[i].name[0];
                if(long_options[i].has_arg > 0)
                    short_options[k++] = ':';
                if(long_options[i].has_arg == 2)
                    short_options[k++] = ':';
            }
        }
        short_options[k] = '\0';
    }

    if(hs_set(HS_MODULE_OPT, &argc, argv) != 0)
    {
        fprintf(stderr, "WARNING: failed to set hyantes module option\n");
    }

    while((opt =
          getopt_long(argc, argv, short_options, long_options,
             &option_index)) != -1)
    {
        if(opt == 0 && long_options[option_index].val != 0)
            opt = long_options[option_index].val;

        switch (opt)
        {
        case 0:
            switch (option_index)
            {
            default:
                fprintf(stderr, "unrecognized option %s\n",
                   long_options[option_index].name);
                print_help(stderr);
                exit(EXIT_FAILURE);
            }

        case 'd':
            do_compute = 0;
            break;

        case 'f':
            function = optarg;
            break;

        case 'h':
            print_help(stdout);
            return 0;

        case 'i':
            if(fi)
            {
                if(fclose(fi) != 0)
                {
                    perror("[fclose]");
                    exit(EXIT_FAILURE);
                }
            }
            printf("opening: %s\n", optarg);
            fi = fopen(optarg, "r");
            if(!fi)
            {
                perror(optarg);
                exit(EXIT_FAILURE);
            }
            break;

        case 'o':
            if(fo != NULL)
            {
                if(fclose(fo) != 0)
                {
                    perror("fclose");
                    exit(EXIT_FAILURE);
                }
            }
            fo = fopen(optarg, "w");
            if(!fo)
            {
                perror(optarg);
                exit(EXIT_FAILURE);
            }
            break;

        case 'p':
            (void) hs_set(HS_LOAD_PRECOMPUTED);
            break;

        case 's':
            {
                char *res = strtok(optarg, "x");
                if(res)
                    resLat = atoi(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
                res = strtok(NULL, "");
                if(res)
                    resLon = atoi(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
            }
            break;

        case 't':
            (void) hs_set(HS_THRESHOLD, atof(optarg));
            break;

        case 'r':
            range = atof(optarg);
            break;

        case 'v':
            print_version(stdout);
            exit(EXIT_SUCCESS);

        case 'w':
            {
                char *res = strtok(optarg, ",");
                if(res)
                    visu.mLat = atof(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
                res = strtok(NULL, ",");
                if(res)
                    visu.mLon = atof(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
                res = strtok(NULL, ",");
                if(res)
                    visu.MLat = atof(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
                res = strtok(NULL, "");
                if(res)
                    visu.MLon = atof(res);
                else
                {
                    print_help(stderr);
                    exit(EXIT_FAILURE);
                }
            }
            break;

        default:
            exit(EXIT_FAILURE);
        }
    }

    if(fi == NULL)
    {
        fprintf(stderr, "no input file given, using stdin\n");
        fi = stdin;
    }
    if(fo == NULL)
    {
        fprintf(stderr, "no output file given, using stdout\n");
        fo = stdout;
    }
    if(do_compute == 0)
    {
        (void) hs_set(HS_PARSE_ONLY, fo);
    }

    if(resLat == 0)
    {
        fprintf(stderr,
           "no latitude resolution given, defaulting to `100'\n");
        resLat = 100;
    }
    if(resLon == 0)
    {
        fprintf(stderr,
           "no longitude resolution given, defaulting to `100'\n");
        resLon = 100;
    }
    if(fabs(visu.mLat) < FLT_EPSILON && fabs(visu.mLon) < FLT_EPSILON
       && fabs(visu.MLat) < FLT_EPSILON && fabs(visu.MLon) < FLT_EPSILON)
    {
        fprintf(stderr, "no visualisation window given, exiting\n");
        exit(EXIT_FAILURE);
    }
    if(function == NULL)
    {
        fprintf(stderr, "no function given , defaulting to `disk'\n");
        function = (char *) default_function;
    }
    if(fabs(range) < DBL_EPSILON)
    {
        fprintf(stderr, "no function range given, defaulting to `10'\n");
        range = 10.f;
    }

    /* main call */
#if HAVE_LIBPTHREAD
    sz = resLat * resLon;
    /*@-compdef@ *//*@-nullpass@ */
    if(do_compute == 1
       && pthread_create(&thread, NULL, (void *(*)(void *)) display_status,
          &sz) != 0)
    {
        perror("[pthread_create]");
        exit(EXIT_FAILURE);
    }
#endif

    if(function != NULL && (range > FLT_EPSILON))
        (void) hs_set(HS_SMOOTH_FUNC, function, range);

    pOut = hs_smooth(resLat, resLon, visu, fi);
	fclose(fi);


    /* catch error */
    if(do_compute == 1)
    {
        int k,l;
        if(pOut == NULL)
        {
            fprintf(stdout, "# program ended with an error\n");
            free(pOut);
            return 0;
        }

        /* write result on stdout */
        for(k = 0; k < resLat; k++)
        {
            for(l = 0; l < resLon; l++)
                fprintf(fo, "%lf %lf %lf\n", pOut[k * resLon + l].lat,
                   pOut[k * resLon + l].lon, pOut[k * resLon + l].pot);
            fprintf(fo, "\n");
        }

        /* the end */
        free(pOut);
    }
    else
    {
        assert(pOut == NULL);
    }
#if HAVE_LIBPTHREAD
    /*@-usedef@ */
    if(do_compute == 1 && pthread_join(thread, NULL) != 0)
    {
        perror("[pthread_join]");
        exit(EXIT_FAILURE);
    }
#endif

    if(fclose(fo) != 0)
    {
        perror("[fclose]");
        exit(EXIT_FAILURE);
    }
    return 0;
}

int main(int argc, char **argv)
{
    int retno;
    retno = run(argc, argv);
    return retno;
}
