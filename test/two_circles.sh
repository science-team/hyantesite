TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
../bin/hyantesite --window=-1,-3,1,3 --function=amortized_disk --range=10 --scale=80x80 --in=$TOP_SRCDIR/test/two_circles.in --out=$TMPFILE
if cmp $TOP_SRCDIR/test/two_circles.out $TMPFILE
then
    rm -f $TMPFILE
    exit 0
else
	echo "$TMPFILE seems wrong"
    exit 1
fi

