TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
../bin/hyantesite --window=-1,-3,1,3 --function=amortized_disk --range=100 --threshold 0 --scale=100x100 --in=$TOP_SRCDIR/test/two_large_circles.in --out=$TMPFILE
if cmp $TOP_SRCDIR/test/two_large_circles.out $TMPFILE
then
    rm -f $TMPFILE
    exit 0
else
	echo "$TMPFILE seems wrong"
    exit 1
fi

