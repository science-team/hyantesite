/*
 * This file is part of hyantes.
 *
 * hyantes is free software; you can redistribute it and/or modify
 * it under the terms of the CeCILL-C License
 *
 * You should have received a copy of the CeCILL-C License
 * along with this program.  If not, see <http://www.cecill.info/licences>.
 */

/*
 * This file is part of hypersmooth.
 *
 * hypersmooth is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * hypersmooth is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <stdlib.h>

#include "hyantes.h"

int main(int argc, char **argv)
{
    const char **funs = NULL;
    size_t i, sz = 0;
    funs = hs_list_smoothing(&sz);
    for(i = 0; i < sz; i++)
        printf("%s\n", funs[i]);
    exit(EXIT_SUCCESS);
}
