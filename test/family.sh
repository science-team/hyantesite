TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
../bin/hyantesite --window=0,0,10,10 --threshold 0 --function=amortized_disk --range=20 --scale=1000x1000 --in=$TOP_SRCDIR/test/family.in --out=$TMPFILE
if cmp $TOP_SRCDIR/test/family.out $TMPFILE
then
    rm -f $TMPFILE
    exit 0
else
	echo "$TMPFILE seems wrong"
    exit 1
fi

