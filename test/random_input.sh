# this test creates a random input and run it
# if no crashes, the test is ok

TMPINFILE=$(mktemp -t libhypersmooth.XXXXXX)
for i in `seq 1 10000`
do
cat >> $TMPINFILE << EOF
$(($RANDOM % 90)).$(($RANDOM)) $(($RANDOM % 90)).$(($RANDOM))  $(($RANDOM % 200)).$(($RANDOM)) 
EOF
done

TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
if ../bin/hyantesite -w 0,0,90,90 -f disk -r 100 -s 100x100 -i $TMPINFILE -o $TMPFILE
then
    rm -f $TMPFILE $TMPINFILE
    exit 0
else
    echo "see $TMPFILE and $TMPINFILE"
    exit 1
fi

