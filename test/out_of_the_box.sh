TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
../bin/hyantesite --window=0.1,0.1,1,1 --function=amortized_disk --range=20 --scale=100x100 --in=$TOP_SRCDIR/test/borderline.in --out=$TMPFILE
if cmp $TOP_SRCDIR/test/out_of_the_box.out $TMPFILE
then
    rm -f $TMPFILE
    exit 0
else
	echo "$TMPFILE seems wrong"
    exit 1
fi

