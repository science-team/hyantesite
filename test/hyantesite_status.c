/*
 * This file is part of hyantes.
 *
 * hyantes is free software; you can redistribute it and/or modify
 * it under the terms of the CeCILL-C License
 *
 * You should have received a copy of the CeCILL-C License
 * along with this program.  If not, see <http://www.cecill.info/licences>.
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include "hyantes.h"

/******************************************************************************\
*
*               main
*
\******************************************************************************/

static int run(int argc, char **argv)
{
    int i, len;
    int resLat, resLon;
    hs_potential_t *pOut = NULL;
    hs_coord_t visu;
    FILE *fi = NULL;

    unsigned long before, after;

    /* visu = { latmin, longmin, latmax, longmax } */
    visu.mLat = atof(argv[1]);
    visu.mLon = atof(argv[2]);
    visu.MLat = atof(argv[3]);
    visu.MLon = atof(argv[4]);

    /* open data file */
    fi = fopen(argv[9], "r");
    if(fi == NULL)
    {
        perror("[main::fopen] ");
        fprintf(stderr, "while opening '%s' \n", argv[9]);
        return 1;
    }

    /* main call */
    before = hs_status();
    pOut = hs_smoothing(resLat = atoi(argv[7]), /* resLat */
       resLon = atoi(argv[8]),  /* resLon */
       argv[5],                 /* fonction */
       atof(argv[6]),           /* param fonction */
       visu,                    /* param visu */
       fi                       /* in */
       );

    /* catch error */
    if(pOut == NULL)
    {
        fprintf(stdout, "# program ended with an error\n");
        free(pOut);
        return 0;
    }
    after = hs_status();


    /* the end */
    free(pOut);
    printf("resLat*resLon=%d\n", resLat * resLon);
    printf("before=%d and after=%d\n", before, after);
    return !((before == 0) && (after == resLat * resLon));
}

int main(int argc, char **argv)
{
    int retno;
    retno = run(argc, argv);
    return retno;
}
