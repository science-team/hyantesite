#!/bin/sh
TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
../bin/hyantesite --window=1,32,4,35 --function=gaussian -t 0.0000001 -r 15  -s 160x80 --in=$TOP_SRCDIR/test/ra.in --out=$TMPFILE
if cmp $TOP_SRCDIR/test/ra_gaussian.out $TMPFILE
then
    rm -f $TMPFILE
    exit 0
else
	echo "$TMPFILE seems wrong"
    exit 1
fi
