# this test creates a random input and run it
# if no memory leak, test is ok

if ! which valgrind > /dev/null
then
    echo "valgrind not in PATH, test desactivated"
    exit 0
fi

TMPINFILE=$(mktemp -t libhypersmooth.XXXXXX)
for i in `seq 1 1000`
do
cat >> $TMPINFILE << EOF
$(($RANDOM % 90)).$(($RANDOM)) $(($RANDOM % 90)).$(($RANDOM))  $(($RANDOM % 200)).$(($RANDOM)) 
EOF
done

TMPFILE=$(mktemp -t libhypersmooth.XXXXXX)
valgrind --leak-check=full --show-reachable=yes ../bin/.libs/hyantesite -w 0,0,90,90 -f disk -r 100 -s 100x100 -i $TMPINFILE -o /dev/null 2>&1 | tee $TMPFILE | sed -n -e '/SUMMARY/,$ p' 

if grep "no leaks are possible" $TMPFILE > /dev/null 
then
    rm -f $TMPFILE $TMPINFILE
    exit 0
else
    exit 1
fi

