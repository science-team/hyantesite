#!/bin/sh
# generate missing texinfo file from AUTHORS or THANKS

set -e

#fix pwd
cd `echo $0 | sed s/gen-missing.sh//`

f=$1

SRC=../`basename $f .texi`
if test -f $SRC ; then
    sed -r -e "s/<([^@]+)@([^>]+)>/@email{\1 @@ \2 }/g"\
    -e "s#\`([^']+)'#@code{\1}#g" \
    -e "1,2 d"\
    -e "/list/a @itemize"\
    -e "$ a @end itemize"\
    -e "s/ - ([^@]+)/ - @emph{\1}/g"\
    -e "/^$/d"\
    $SRC | \
    sed -r -e "/@itemize/,/@end itemize/ s/^([^@])/@item \1/"
fi
