#!/usr/bin/perl
use strict;
use warnings;

my $usage = "usage: shadoc.pl <header> <source> <another_source> ...\n";
if( $#ARGV +1 < 2 ) { die $usage; }

# libname
$ARGV[0]=~/(?:.*\/)*(.+)/;
my $libname=$1;

# read header file
open INPUT ,$ARGV[0] or die "cannot open $ARGV[0]:$!";
my @lines=<INPUT>; 
my $header = join "\n", @lines;
close INPUT;

# read source files
my $source;
for( my $i = 1 ; $i < $#ARGV +1 ; $i++)
{
    open INPUT ,$ARGV[$i] or die "cannot open $ARGV[$i]:$!";
    @lines=<INPUT>; 
    $source .= join "\n", @lines;
    close INPUT;
}

# tokens
my $doxygen = '(?:\/\*\*[^*]*\*+(?:[^\/*][^*]*\*+)*\/)';
my $ws = '(?:\s|\/\*[^*]*\*+(?:[^\/*][^*]*\*+)*\/)*';
my $word = '[a-zA-Z_]\w*';
my $type = '(?:(?:const|volatile|signed|unsigned)\s+)*';
my $scope = '(?:extern|static)?';


# print intro
print <<EOF
\@c ---------------
\@node Library Interface
\@chapter Library Interface

All following \@samp{C} enumerations, structures and functions can be found in the header file \@file{$libname}.
To use them, simply include it
\@example
    #include <$libname>
\@end example

\@menu
* Enumerations :: enumeration available to \@file{$libname} users
* Data Structures :: structures  available to \@file{$libname} users
* User Functions :: functions available to \@file{$libname} users
\@end menu

EOF
;

# find structures / enums in header

print <<EOF
\@c ---------------
\@node Enumerations
\@section Enumerations

EOF
;
# the regexp -- extract enums
my @enums=($header=~/(?:typedef)?${ws}enum$ws(?:$word)?$ws\{[^\}]+\}$ws(?:$word)?$ws;/gm);

# process each element
foreach (@enums)
{
    /(?:typedef)${ws}enum$ws((?:$word)?)$ws\{([^\}]+)\}$ws((?:$word)?)$ws/;
    my $name=$1?"enum $1":'@strong{unamed enumeration}';
    my $content=$2;
    my $typedef=$3;
    my @elements=($content=~/$ws($word)$ws(?:\=$ws\w$ws)?,?/gm);

    # print header
    print "\@deftp {Data Type} {$name}";
    foreach (@elements) { print " $_" ; }
    print "\n";

    # print content header
    print "\@vtable \@code\n";

    foreach my $elem (@elements)
    {
        # get fields
        $content=~/$elem\s*((?:\=$ws\w$ws)?),?\s*($doxygen?)/gm;
        my $value = $1;
        my $comment = $2;

        # print data
        print "\@item $elem\n";
        if( $value ) {print "\@strong{Value}: $value\n";}
        if( $comment )
        {
            (my $fcomment = $comment ) =~s/(\/\*\*<)|(\*\/)//gm;
            print "$fcomment\n";
        }
        else
        {
            print "no comment given\n" ;
        }
    }
    # print content footer
    print "\@end vtable\n";
    print "\@end deftp\n"
}

print <<EOF
\@c ---------------
\@node Data Structures
\@section Data Structures

EOF
;

# the regexp -- extract structs
my @structs=($header=~/(?:typedef)?${ws}struct$ws(?:$word)?$ws\{[^\}]+\}$ws(?:$word)?$ws;/gm);

# process each element
foreach (@structs)
{
    /(?:typedef)${ws}struct$ws((?:$word)?)$ws\{([^\}]+)\}$ws((?:$word)?)$ws/;
    my $name=$1?"struct $1":'@strong{unamed structure}';
    my $content=$2;
    my $typedef=$3;
    my @elements=($content=~/$ws$scope$ws$type$word[\s\*]+($word)$ws;/gm);

    # print header
    my $stype = $typedef?$typedef:$name;
    print "\@deftp {Data Type} {$stype}";
    foreach (@elements) { print " {$_}" ; }
    print "\n";

    # print content header
    print "\@vtable \@code\n";

    foreach my $elem (@elements)
    {
        # get fields
        if( $content=~/($scope$ws$type$ws$word[\s\*]+)$elem$ws;\s*($doxygen?)/gm )
        {
            my $ftype = $1;
            my $comment = $2;
            $ftype=~s/\s+/ /gm;

            # print data
            print "\@item $elem\n";
            if( $ftype ) {print "\@strong{Type}: $ftype\n";}
            if( $comment )
            {
                (my $fcomment = $comment ) =~s/(\/\*\*<)|(\*\/)//gm;
                print "$fcomment\n";
            }
            else
            {
                print "no comment given\n" ;
            }
        }
        else
        {
            die "unable to match field $elem in structure $name";
        }
    }
    # print content footer
    print "\@end vtable\n";
    print "\@end deftp\n"
}

# find functions in header

print <<EOF
\@c ---------------
\@node User Functions
\@section User Functions

EOF
;

# the regexp -- extract function names
my @funcs=($header=~/$scope$ws$type$word[\s\*]+($word)$ws\([^)]*\)$ws;/gm);

# process each function
foreach my $name (@funcs)
{
    # search the function in the source file
    if($source=~/($doxygen)$ws($scope$ws$type$word[\s\*]+$name$ws\([^)]*\))$ws\{/)
    {
        # get parts
        my $dox = $1;
        my $body = $2;
        
		# parse body
        $body=~m/$scope$ws($type$word[\s\*]+)$name$ws(\([^)]*\))/;
        my $return = $1;
        my $param = $2;

        # reformat body part
        $param=~s/\s+/ /gm;
        $param=~s/\/\*[^*]*\*+(?:[^\/*][^*]*\*+)*\///g;
        (my $formated_param = $param) =~s/($word)\s*([,\)])/\@var\{$1\}$2/g;
        $return=~s/\s+/ /gm;

        #print header
        print "\@deftypefn {Library Function} {$return} {$name} $formated_param\n";


        # parse dox && print body
        $dox=~s/\/\*\*+|\*\/|\@brief//gm;
        $dox=~s/([\n\r])\s*\*/$1/gm;
        if( $dox=~m/([^\@]*)((?:\@param\s[^@]+)*)(?:\@return\s(.*))?/m )
        {
            my $intro = $1;
            my $_params = $2;
            my $return = $3;

            # print main descr
            print "$intro\n";

            # print arg descr
            if( $_params )
            {
                my @params = ($_params=~/\@param\s+$word\s[^@]+/gm);
                print "\@table \@var\n";
                foreach (@params)
                {
                    /\@param\s+($word)\s([^@]+)/m;
                    print "\@item $1\n$2\n";
                }
                print "\@end table\n";
            }
            #print return value if any
            if( $return )
            {
                print "\@strong{Return value}: $return\n"
            }

            # print footer
            print "\@end deftypefn\n";
        }

    }
    else
    {
        my $msg = "function \`$name\' not found in $ARGV[1]";
        for( my $i = 2; $i < $#ARGV +1 ; $i++)
        {
            $msg.="or $ARGV[$i]";
        }
        die "$msg";
    }
}

