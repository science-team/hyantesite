\input texinfo
@c %** start of header
@setfilename hyantes.info
@settitle hyantes 
@include version.texi
@dircategory Software libraries
@direntry
* Hyantes: (hyantes).       library to compute neighbourhood population potential with scale control 
@end direntry
@c %** end of header

@copying
This manual is for Hyantes (version @value{VERSION}, @value{UPDATED}),
Hyantes is free software; you can redistribute it and/or modify
it under the terms of the CeCILL-C License

Copyright @copyright{} 2007 LIG
@end copying

@titlepage
@title Hyantes 
@subtitle for version @value{VERSION}, @value{UPDATED}
@author L. S. Guelton (@email{serge.guelton@@enst-bretagne.fr})
@page
@insertcopying
@end titlepage

@contents

@ifnottex
@node Top
@top Hyantes
This installation manual is for Hyantes library (version @value{VERSION}, @value{UPDATED}).
@end ifnottex

@menu
* Installation Instructions  :: How to build and install Hyantes on your system 
* Overview :: Detailed overview of Hyantes content
* Library Interface :: functions exported by the Hyantes library
* hyantesite :: sample program using the hyantes library
* Examples :: to make everything easier to understand
* Contacts :: flame or grats the authors
@end menu

@include install.texi
@include overview.texi
@include interface.texi
@include hyantesite.texi
@include examples.texi
@include contact.texi

@bye
