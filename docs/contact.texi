@c ---------------
@node Contacts
@chapter Contacts

@menu
* LIG ::
* MESCAL ::
* Authors ::
* Thanks ::
@end menu

@node LIG
@section LIG

@code{Hyantes} is developed in a team from the 
@display
Laboratoire LIG
110 av. de la Chimie - Domaine Universitaire
BP 53 - 38041 Grenoble - France cedex 9
Tel: 04 76 51 43 61
@end display

@node MESCAL
@section MESCAL

The team itself is known as @url{http://mescal.imag.fr,Mescal}.
@display
Laboratoire Informatique et Distribution (ID)-IMAG
ENSIMAG - antenne de MontbonnotZIRST 51, avenue Jean Kuntzmann
38330 Montbonnot Saint Martin / France
@end display

@node Authors
@section Authors

@include AUTHORS.texi

@node Thanks
@section Thanks

@include THANKS.texi
